class StatPage:
    
    def __init__(self, year, week, team, opp, stat_dict):
        self.year = year
        self.week = week
        self.team = team
        self.opp = opp
        self.stats = stat_dict

    def __str__(self):
        stringified = self.team + " vs. " + self.opp + "\n"\
            + "Week " + str(self.week) + " " + str(self.year) + "\n"
        for key, value in self.stats.items():
            stringified += "\n" + str(key) + ":\t\t" + str(value)

        return stringified 
    
    # Getting an error since the header of the passing categories is just passing, rest are unnamed
    def get_passing_stats(self):
        passing_categories = ["OPP", "CMP", "ATT", "PCT", "YDS", "AVG", "TD", "INT"]
        passing_stats = {}
        for category in passing_categories:
            passing_stats[category] = float(self.stats[category])
        return passing_stats

    def get_fantasy_stats(self):
        fantasy_stats = {}
        for category in ["PPG", "FPTS"]:
            fantasy_stats[category] = float(self.stats[category])
        return fantasy_stats