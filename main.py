from player import Player
from statPage import StatPage
from pandas import read_excel

DATA_FOLDER = "/media/matthew/4CFC296FFC29550C/Fantasy Data/"

def get_weekly_player_data(player_name, pos, year, start_week, end_week):

    # Validate input weeks and position are valid
    # TODO

    # Set the location of the data
    base_dir = DATA_FOLDER + "Player Game Stats/" + str(year) + "/"

    # Iterate through the folder, collecting all data into the player object
    player = Player(player_name, pos)
    for week in range(start_week, end_week + 1):
        stats = {}
        filename = base_dir + str(year) + " Week " + str(week) + " " + pos + ".xlsx"
        df = read_excel(filename, sheet_name=None)
        player_row = df["Sheet1"][df["Sheet1"]["Name"] == player_name]
        for stat, value in player_row.items():
            if stat is not "Unnamed":
                stats[stat] = value
        player.add_game(year, week, stats["Team"], stats["OPP"], stats)
    return player




##################################### Tests #############################################
player = get_weekly_player_data("Michael Vick", "QB", 2002, 1, 3)
print(player.games[0].get_fantasy_stats())