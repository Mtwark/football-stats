from statPage import StatPage

class Player:

    def __init__(self, name, pos):
        valid_positions = ["QB", "RB", "WR", "TE", "K", "DEF"]
        self.name = str(name)
        if pos not in valid_positions:
            raise(ValueError)
        self.pos = str(pos)
        self.games = []

    def __str__(self):
        stringified = "Player:\t\t" + self.name + "\nPosition:\t"\
            + self.pos + "\nGames:\t\t" + str(len(self.games))
        return stringified
    

    def add_game(self, year, week, team, opp, stat_dict):
        stats = StatPage(year, week, team, opp, stat_dict)
        self.games.append(stats)
